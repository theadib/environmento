
environmento scanner
====================

Scanner scans for environmento sensors.
Displays data from sensor

basics
======

```
> hcitool dev
  Devices:
	hci0	5C:F3:70:70:D9:EF

> hcitool lescan
  LE Scan ...
  D1:7E:6A:F8:F9:E9 Thingy

> gatttool -b D1:7E:6A:F8:F9:E9 -I -t random
  [D1:7E:6A:F8:F9:E9][LE]> 
```


configuring
===========

how to not use sudo when doing scanning

@see https://unix.stackexchange.com/questions/96106/bluetooth-le-scan-as-non-root

```
> sudo setcap 'cap_net_raw,cap_net_admin+eip' `which hcitool`
```

persistency
===========

Data are stored to disk using InfluxDB.

Data can be exported to CSV

Database location on the disk is 
```
/var/lib/influxdb/data/environmento
```

