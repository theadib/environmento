#!/usr/bin/env python3

import asyncio
from bleak import BleakScanner, BleakClient
import binascii
import struct
import datetime

string_characteristics = []
bin_characteristics = []

''' (service-uuid,characteristic-uuid),(struct-format,[keynames],is_dynamic) '''

def decode_characterstics(uuid, data):
    ''' decode the bytearray(data) according to characteristics (service-uuid, characteristics-uuid) '''
    formats = {
            ('0000180a-0000-1000-8000-00805f9b34fb','00002a26-0000-1000-8000-00805f9b34fb'):('<{}s'.format(len(data)),['revision'],False),    # firmware revision
            ('0000180a-0000-1000-8000-00805f9b34fb','00002a29-0000-1000-8000-00805f9b34fb'):('<{}s'.format(len(data)),['manufacturer'],False),    # manufacturer name
            ('0000180a-0000-1000-8000-00805f9b34fb','00002a24-0000-1000-8000-00805f9b34fb'):('<{}s'.format(len(data)),['model'],False),    # model number
            ('0000180f-0000-1000-8000-00805f9b34fb','00002a19-0000-1000-8000-00805f9b34fb'):('<B',['battery'],True),    # battery level %
            ('12345678-1234-5678-1234-56789abcdef0','12345678-1234-5678-1234-56789abcdef1'):('<H',['co2'],True),    # co2 
            ('12345678-1234-5678-1234-56789abcdef0','12345678-1234-5678-1234-56789abcdef2'):('<H',['voc'],True),    # voc
            ('12345678-1234-5678-1234-56789abcdef0','12345678-1234-5678-1234-56789abcdef3'):('<h',['temperature'],True),    # temperature
            ('12345678-1234-5678-1234-56789abcdef0','12345678-1234-5678-1234-56789abcdef4'):('<h',['humidity'],True),    # humidity
            ('12345678-1234-5678-1234-56789abcdef0','12345678-1234-5678-1234-56789abcdef5'):('<HHhhbhhhh',['co2','voc','temperature','humidity','position','light_red','light_green','light_blue','light_ir'],True),    # co2, voc, temperature, humidity, shortId, light[4]
            ('12345678-1234-5678-1234-56789abcdef0','12345678-1234-5678-1234-56789abcdef6'):('<B',['position'],False)     # short ID
        }
    if uuid in formats:
        try:
            values = struct.unpack(formats[uuid][0], data)
            keys = formats[uuid][1]
        except Exception as e:
            values = [data.decode('utf-8') + ' - ' + str(binascii.hexlify(data)) + ' ERR: ' + str(e)]
            keys = ['generic error']
    else:
        keys = ['generic']
        try:
            values = [data.decode('utf-8') + ' - ' + str(binascii.hexlify(data))]
        except:
            values = [str(binascii.hexlify(data))]
    
    return dict(zip(keys, values))

def tagnames():
    return ['revision', 'manufacturer', 'model', 'position']

def fieldames():
    return ['co2', 'voc', 'temperature', 'humidity', 'battery', 'light_red','light_green','light_blue','light_ir']

'''
    5F:04:E0:FA:31:EF: Environmento
    connect
    {'uuids': ['0000180d-0000-1000-8000-00805f9b34fb', '0000180f-0000-1000-8000-00805f9b34fb', '00001805-0000-1000-8000-00805f9b34fb', '12345678-1234-5678-1234-56789abcdef0'], 'manufacturer_data': {}}
    connected
    service:  12345678-1234-5678-1234-56789abcdef0
    characteristic:  12345678-1234-5678-1234-56789abcdef6 ['read', 'write'] (0,)
    characteristic:  12345678-1234-5678-1234-56789abcdef5 ['read'] (1095, 105, 226, 375, 0, 60, 210, 24, 23)
    characteristic:  12345678-1234-5678-1234-56789abcdef4 ['read'] (375,)
    characteristic:  12345678-1234-5678-1234-56789abcdef3 ['read'] (226,)
    characteristic:  12345678-1234-5678-1234-56789abcdef2 ['read'] (105,)
    characteristic:  12345678-1234-5678-1234-56789abcdef1 ['read'] (1095,)
    service:  0000180a-0000-1000-8000-00805f9b34fb
    characteristic:  00002a26-0000-1000-8000-00805f9b34fb ['read'] 0.0.1
    characteristic:  00002a29-0000-1000-8000-00805f9b34fb ['read'] theAdib
    characteristic:  00002a24-0000-1000-8000-00805f9b34fb ['read'] Thingy:52
    service:  00001805-0000-1000-8000-00805f9b34fb
    characteristic:  00002a2b-0000-1000-8000-00805f9b34fb ['read', 'write', 'notify'] b'df07051e0c2d1e010000'
    service:  0000180f-0000-1000-8000-00805f9b34fb
    characteristic:  00002a19-0000-1000-8000-00805f9b34fb ['read', 'notify'] (47,)
    service:  00001801-0000-1000-8000-00805f9b34fb
    characteristic:  00002b2a-0000-1000-8000-00805f9b34fb ['read'] b'e4f08474b43613a4296d702089608a70'
    characteristic:  00002b29-0000-1000-8000-00805f9b34fb ['read', 'write']  - b'05'
    characteristic:  00002a05-0000-1000-8000-00805f9b34fb ['indicate'] 
'''

async def run():
    results = []
    print('init')
    scanner = BleakScanner()
    print('start')
    await scanner.start()    
    print('sleep')
    await asyncio.sleep(5.0)
    print('stop')
    await scanner.stop()    
    print('sleep')
    await asyncio.sleep(1.0)
    print('discover')
    devices = await scanner.get_discovered_devices()
        
    for d in devices:
        print(d)
        if 'Environmento' in d.name:
            print('connect')
            print(d.metadata)
            client = BleakClient(d.address)

            try:
                values = {'uuid':d.address}
                await client.connect()
                print('connected')
                # services = await client.get_services()
                for service in client.services:
                    print("service: ", service.uuid)
                    for characteristic in service.characteristics:
                        if "read" in characteristic.properties:
                            try:
                                data = bytes(await client.read_gatt_char(characteristic.uuid))
                                value = decode_characterstics((service.uuid,characteristic.uuid), data)
                                print('update: ', values, value)
                                values.update(value)
                            except Exception as e:
                                value = str(e)
                        else:
                            value = ''
                        print("  characteristic: ", characteristic.uuid, characteristic.properties, value)
                        
            except Exception as e:
                print(e)
            finally:
                print(values)
                await client.disconnect()
                if len(values):
                    results.append(values)
    return results

if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    device_results = loop.run_until_complete(run())

    print(device_results)
    print(tagnames())
    print(fieldames())
