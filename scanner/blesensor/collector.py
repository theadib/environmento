#!/usr/bin/env python3

import asyncio
import time
import blesensor
import storage
import datetime

def run():
    try:
        loop = asyncio.get_event_loop()
        device_results = loop.run_until_complete(blesensor.run())

        db = storage.Storage()
        db.init(defaultdatabase='environmento', defaulttable='environment', keys=blesensor.fieldames(), tags=blesensor.tagnames())
        for r in device_results:    
            db.push(r)
        db.report()

    except Exception as e:
        print('## UUPS ## {}: {}'.format(datetime.datetime.now().isoformat(), str(e)))

if __name__ == "__main__":

    while(1):
        run()
        time.sleep(60.0)
