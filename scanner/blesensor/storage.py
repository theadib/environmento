#!/usr/bin/env python3 

''' storage class for environmento project based on InfluxDB '''


import sys
import datetime
from influxdb import InfluxDBClient
# from pandas import pd
import csv


class Storage:
    def __init__(self):
        self.client = InfluxDBClient(host='localhost', port=8086)
        self.databasename = ''
        self.tablename = ''
        self.fieldnames = ''
        self.tagnames = ''

    def init(self, defaultdatabase='', defaulttable='', keys='', tags=''):
        self.databasename = defaultdatabase
        self.tablename = defaulttable
        self.fieldnames = keys
        self.tagnames = tags
        print(self.client.get_list_database())
        if self.databasename not in [i['name'] for i in self.client.get_list_database()]:
            print('create database')
            self.client.create_database(self.databasename)
        print(self.client.get_list_database())
        self.client.switch_database(self.databasename)

    def push(self, data, tablename=''):
        ''' push the dictionary data into store '''
        '''
        {'measurement': 'environment', 
            'tags': {'position': '3', 'uuid': '123'}, 
            'time': '2021-01-04T00:52:40.872321', 
            'fields': {'co2': 1, 'voc': 1}
        }
        '''
        if not tablename:
            tablename = self.tablename
        json_body = [
            {
                "measurement": tablename,
                "tags": {},
                "time": data.get('time', datetime.datetime.now().isoformat()),
                "fields": {}
            },
        ]
        for field in self.fieldnames:
            if field in data:
                json_body[0]['fields'][field] = data.get(field, '')
        for tag in self.tagnames:
            if tag in data:
                json_body[0]['tags'][tag] = data.get(tag, '')

        print(json_body)
        self.client.write_points(json_body)

    def report(self, tablename=''):
        if not tablename:
            tablename = self.tablename
        # results = self.client.query('SELECT "*" FROM environment WHERE time > now() - 4d GROUP BY "position"')
        results = self.client.query('SELECT * FROM {}'.format(tablename))
        print(results.raw)
        if 'series' in results.raw and len(results.raw['series']):
           print(results.raw['series'][0]['columns'])
           for row in results.raw['series'][0]['values']:
               print(row)
        print('get_points():')
        '''
        {'time': '2021-01-04T00:11:50.438095104Z', 'co2': 1, 'device_id': '0', 'position': '0', 'voc': 1}
        {'time': '2021-01-04T00:12:03.341213952Z', 'co2': 1, 'device_id': '0', 'position': '0', 'voc': 1}
        {'time': '2021-01-04T00:12:08.700144128Z', 'co2': 1, 'device_id': '0', 'position': '0', 'voc': 1}
        '''
        for points in results.get_points():
            print(points)
        print('items:')
        for item in results.items():
            print(item[0], list(item[1]))
        print('keys: ', results.keys())

        if 'series' in results.raw and len(results.raw['series']):
            with open(self.tablename+'.csv', 'wt') as f:
                writer = csv.DictWriter(f, results.raw['series'][0]['columns'])
                writer.writeheader()
                writer.writerows(results.get_points())



def main(args):
    print(args)

    store = Storage()
    store.init(defaultdatabase='environmento', defaulttable='environment', 
            keys=['co2', 'voc', 'temperature', 'humidity', 'voltage', 'light_red', 'light_green', 'light_blue', 'light_ir'],
            tags=['position', 'uuid'])
    store.push({'co2':1, 'voc':1, 'uuid':'123', 'position':'3'})
    store.report()
    return 1


if __name__ == "__main__":
    sys.exit(main(sys.argv))

