Environmento
============

System for measure and analyse environmental data.
The system gives feedback.

Advanced "CO2 Ampel".

goals
-----

- monitor work space environment
- give feedback for ventilation

general
-------

- Thingy::52, system is build from Nordic development system NRF52832, www.nordicsemi.com
- CCS811, metal oxide gas sensor for volotile organic compunds VOC),
  calculates equivalent eTVOC and eCO2, https://www.sciosense.commetal
- HTS211 for measuring humidity and temperature, www.st.com
- BH1745NUC for measuring light intensity, www.rohm.com

system overview
---------------

- sensor and light
- collector
- view 

other sensors
-------------

### Thingy::91
- NRF9160, NRF52840, https://www.nordicsemi.com/Software-and-tools/Prototyping-platforms/Nordic-Thingy-91
- BME680 Bosch Environmental Sensor, gas VOC, pressure, humidity, temperature, https://www.bosch-sensortec.com/products/environmental-sensors/gas-sensors-bme680/


user feedback for ventilation
-----------------------------

### Hartmann - hartmann_kriegel_2020_de

- Grün: bis 800ppm
- Grün: bis 1000ppm
- Gelb: bis 1500ppm
- Rot: bis 2000 ppm 
- Rot-blinkend: über 2000 ppm

### https://www.luftlicht.de

CO2 Ampel Grenzwerte: https://www.luftlicht.de/?gclid=Cj0KCQiA5bz-BRD-ARIsABjT4nihV8E_QJ8PhVXqDPg8cM9RpdW6hfKew55UlUXgTEyqcVfCEEFO5XcaAp8vEALw_wcB

CO2-Sensor SCD30 von Sensirion

Die intuitive Farbgebung über das360° Lichtsignalsgibt dabei in vier Abstufungen eine 
Handlungsempfehlung zum Lüften. 
Die Farbgebung des LuftLichts orientiert sich an 
der DIN 1946-6 und der Pettenkofer Kurve. Diese sind:
- Grün: bis999ppm
- Gelb: 1000ppm bis 1999ppm
- Rot: ab2000 ppm 
- Rot-blinkend(optional): über 3000 ppm

### Watterott CO2 Ampel

https://learn.watterott.com/de/breakouts/co2-ampel/getting-started/

SCD30 CO2-Sensor

zur Covid-Prävention
- bis 799 ppm -> grün
- 800 bis 999 ppm -> gelb
- 1000 bis 1199 ppm -> rot
- 1200 bis 1399 ppm -> rotes Blinken
- 1400 ppm und höher -> rotes Blinken + Buzzer

gegen Ermüdung
- bis 999 ppm -> grün
- 1000 bis 1199 ppm -> gelb
- 1200 bis 1399 ppm -> rot
- 1400 bis 1599 ppm -> rotes Blinken
- 1600 ppm und höher -> rotes Blinken + Buzzer
    

publications
------------

https://www.elektroniknet.de/messen-testen/sensorik/sensorik-zur-raumluftueberwachung.181312.html

https://www.tga-fachplaner.de/raumlufttechnik/raumlufttechnische-anlagen-lueftungsbetrieb-corona-zeiten

https://depositonce.tu-berlin.de/bitstream/11303/11477/5/hartmann_kriegel_2020_de.pdf

https://www.elektronikpraxis.vogel.de/aerosol-fruehwarnsystem-usb-datenlogger-als-co2-messgeraet-a-985066/?cmp=nl-388&uuid=43C8FE14-C048-4C47-B5388C348AF2E5E4

https://www.umweltbundesamt.de/sites/default/files/medien/2546/dokumente/irk_stellungnahme_lueften_sars-cov-2_0.pdf


