.. _ble_peripheral:

Bluetooth: Peripheral
#####################

Overview
********

Application acting as environmental station:
- CO2
- VOC
- temperature
- humidity


Requirements
************

* BlueZ running on the host, or
* A board with BLE support

based on 
samples/bluetooth/peripheral
samples/boards/nrf/battery

Building and Running
********************

> source $(ZEPHYR_BASE)/zephyr/zephyr-env.sh
> west build -p auto -b thingy52_nrf52832

This sample can be found under :zephyr_file:`samples/bluetooth/peripheral` in the
Zephyr tree.

See :ref:`bluetooth samples section <bluetooth-samples>` for details.

Hardware
********

NRF52832QFAA-B0 64KRAM, 512KFLASH
  0.0000h-8.0000h, page size 512Bytes
  nvs partition is 7.a000h to 8.0000h total 6000h = 3*512Bytes


TODO
****

[X] proper Battery information
[ ] notification for CO2
[X] persistent storage of sensor position
[ ] user configurable name tag
[X] detect ambient light
[ ] dim LED
[ ] correct device time
[ ] store data sets
[ ] transmit data sets

