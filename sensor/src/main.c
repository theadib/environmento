/* main.c - Application main entry point */

/*
 * Copyright (c) 2015-2016 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 * 
 * 
 *     
 * 
 * ampel settings @see https://learn.watterott.com/breakouts/co2-ampel/getting-started/
   for covid prevention
        up to 799 ppm -> green
        800 to 999 ppm -> yellow
        1000 to 1199 ppm -> red
        1200 to 1399 ppm -> red flashing
        1400 ppm and higher -> red flashing + buzzer

    against tiredness
        up to 999 ppm -> green
        1000 to 1199 ppm -> yellow
        1200 to 1399 ppm -> red
        1400 to 1599 ppm -> red flashing
        1600 ppm and higher -> red flashing + buzzer
 */

#include <zephyr/types.h>
#include <stddef.h>
#include <string.h>
#include <errno.h>
#include <sys/printk.h>
#include <sys/byteorder.h>
#include <zephyr.h>

#include <stdio.h>

#include <settings/settings.h>

#include <device.h>
#include <devicetree.h>
#include <drivers/gpio.h>
#include <drivers/sensor.h>
#include <drivers/i2c.h>

#include <drivers/sensor/ccs811.h>

#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/conn.h>
#include <bluetooth/uuid.h>
#include <bluetooth/gatt.h>
#include <bluetooth/services/bas.h>
#include <bluetooth/services/hrs.h>

#include <drivers/flash.h>
#include <storage/flash_map.h>
#include <fs/nvs.h>


#include "cts.h"
#include "battery.h"

/* Custom Service Variables */
/// uuid of vendor service
static struct bt_uuid_128 vnd_uuid = BT_UUID_INIT_128(
	0xf0, 0xde, 0xbc, 0x9a, 0x78, 0x56, 0x34, 0x12,
	0x78, 0x56, 0x34, 0x12, 0x78, 0x56, 0x34, 0x12);

/// uuid of vendor characteristics
static struct bt_uuid_128 vnd_co2_uuid = BT_UUID_INIT_128(
	0xf1, 0xde, 0xbc, 0x9a, 0x78, 0x56, 0x34, 0x12,
	0x78, 0x56, 0x34, 0x12, 0x78, 0x56, 0x34, 0x12);

static struct bt_uuid_128 vnd_voc_uuid = BT_UUID_INIT_128(
	0xf2, 0xde, 0xbc, 0x9a, 0x78, 0x56, 0x34, 0x12,
	0x78, 0x56, 0x34, 0x12, 0x78, 0x56, 0x34, 0x12);

static const struct bt_uuid_128 vnd_temperature_uuid = BT_UUID_INIT_128(
	0xf3, 0xde, 0xbc, 0x9a, 0x78, 0x56, 0x34, 0x12,
	0x78, 0x56, 0x34, 0x12, 0x78, 0x56, 0x34, 0x12);

static const struct bt_uuid_128 vnd_humidity_uuid = BT_UUID_INIT_128(
	0xf4, 0xde, 0xbc, 0x9a, 0x78, 0x56, 0x34, 0x12,
	0x78, 0x56, 0x34, 0x12, 0x78, 0x56, 0x34, 0x12);

static const struct bt_uuid_128 vnd_combined_uuid = BT_UUID_INIT_128(
	0xf5, 0xde, 0xbc, 0x9a, 0x78, 0x56, 0x34, 0x12,
	0x78, 0x56, 0x34, 0x12, 0x78, 0x56, 0x34, 0x12);

static const struct bt_uuid_128 vnd_sid_uuid = BT_UUID_INIT_128(
	0xf6, 0xde, 0xbc, 0x9a, 0x78, 0x56, 0x34, 0x12,
	0x78, 0x56, 0x34, 0x12, 0x78, 0x56, 0x34, 0x12);

struct vnd_data_t {
	uint16_t co2;	// in ppm [400..29206]
	uint16_t voc;	// in ppb [0..32768]
	int16_t temperature;	// in 0.1deg_C
	int16_t humidity;	// in 0.1*[0..100%]
	uint8_t sid;	// short ID
	uint16_t light[4];	// red, gree, blue, white
} __attribute__((__packed__));
static struct vnd_data_t vnd_data;

typedef enum {
	CO2_GREEN = 1000,
	CO2_YELLOW = 1500,
	CO2_RED = 2000,
	CO2_RED_FLASH = 2001
} co2_levels_t;

static ssize_t read_co2(struct bt_conn *conn, const struct bt_gatt_attr *attr,
			   void *buf, uint16_t len, uint16_t offset)
{
	const char *value = attr->user_data;

	return bt_gatt_attr_read(conn, attr, buf, len, offset, value,
				 sizeof(vnd_data.co2));
}

static ssize_t read_voc(struct bt_conn *conn, const struct bt_gatt_attr *attr,
			   void *buf, uint16_t len, uint16_t offset)
{
	const char *value = attr->user_data;

	return bt_gatt_attr_read(conn, attr, buf, len, offset, value,
				 sizeof(vnd_data.co2));
}

static ssize_t read_temperature(struct bt_conn *conn, const struct bt_gatt_attr *attr,
			   void *buf, uint16_t len, uint16_t offset)
{
	const char *value = attr->user_data;

	return bt_gatt_attr_read(conn, attr, buf, len, offset, value,
				 sizeof(vnd_data.co2));
}

static ssize_t read_humidity(struct bt_conn *conn, const struct bt_gatt_attr *attr,
			   void *buf, uint16_t len, uint16_t offset)
{
	const char *value = attr->user_data;

	return bt_gatt_attr_read(conn, attr, buf, len, offset, value,
				 sizeof(vnd_data.co2));
}

static ssize_t read_combined(struct bt_conn *conn, const struct bt_gatt_attr *attr,
			   void *buf, uint16_t len, uint16_t offset)
{
	const char *value = attr->user_data;

	return bt_gatt_attr_read(conn, attr, buf, len, offset, value,
				 sizeof(vnd_data));
}

static void position_read(uint8_t *, uint8_t);
static void position_write(uint8_t );

static ssize_t read_sid(struct bt_conn *conn, const struct bt_gatt_attr *attr,
			   void *buf, uint16_t len, uint16_t offset)
{
	const char *value = attr->user_data;
	position_read(attr->user_data, 0);

	return bt_gatt_attr_read(conn, attr, buf, len, offset, value,
				 sizeof(vnd_data.sid));
}


static ssize_t write_sid(struct bt_conn *conn,
			      const struct bt_gatt_attr *attr, const void *buf,
			      uint16_t len, uint16_t offset, uint8_t flags)
{
	uint8_t *value = attr->user_data;

	if (flags & BT_GATT_WRITE_FLAG_PREPARE) {
		return 0;
	}

	if (offset + len > sizeof(vnd_data.sid)) {
		return BT_GATT_ERR(BT_ATT_ERR_INVALID_OFFSET);
	}

	memcpy(value + offset, buf, len);

	position_write(*value);

	return len;
}



/* Vendor Primary Service Declaration */
BT_GATT_SERVICE_DEFINE(vnd_svc,
	BT_GATT_PRIMARY_SERVICE(&vnd_uuid),
	// CO2
	BT_GATT_CHARACTERISTIC(&vnd_co2_uuid.uuid,
			       BT_GATT_CHRC_READ,
			       BT_GATT_PERM_READ ,
			       read_co2, NULL, &vnd_data.co2),
	// VOC
	BT_GATT_CHARACTERISTIC(&vnd_voc_uuid.uuid,
			       BT_GATT_CHRC_READ,
			       BT_GATT_PERM_READ ,
			       read_voc, NULL, &vnd_data.voc),
	// temperature
	BT_GATT_CHARACTERISTIC(&vnd_temperature_uuid.uuid,
			       BT_GATT_CHRC_READ,
			       BT_GATT_PERM_READ,
			       read_temperature, NULL, &vnd_data.temperature),
	//humidity
	BT_GATT_CHARACTERISTIC(&vnd_humidity_uuid.uuid, 
				   BT_GATT_CHRC_READ,
			       BT_GATT_PERM_READ,
			       read_humidity, NULL, &vnd_data.humidity),
	// combined co2,voc,temperature, humidity
	BT_GATT_CHARACTERISTIC(&vnd_combined_uuid.uuid,
			       BT_GATT_CHRC_READ,
			       BT_GATT_PERM_READ, 
				   read_combined, NULL, &vnd_data),
	// short_ID
	BT_GATT_CHARACTERISTIC(&vnd_sid_uuid.uuid,
			       BT_GATT_CHRC_READ | BT_GATT_CHRC_WRITE,
			       BT_GATT_PERM_READ | BT_GATT_PERM_WRITE, 
				   read_sid, write_sid, &vnd_data.sid),
);

static const struct bt_data ad[] = {
	BT_DATA_BYTES(BT_DATA_FLAGS, (BT_LE_AD_GENERAL | BT_LE_AD_NO_BREDR)),
	BT_DATA_BYTES(BT_DATA_UUID16_ALL,
		      BT_UUID_16_ENCODE(BT_UUID_HRS_VAL),
		      BT_UUID_16_ENCODE(BT_UUID_BAS_VAL),
		      BT_UUID_16_ENCODE(BT_UUID_CTS_VAL)),
	// UUID of service
	BT_DATA_BYTES(BT_DATA_UUID128_ALL,
		      0xf0, 0xde, 0xbc, 0x9a, 0x78, 0x56, 0x34, 0x12,
		      0x78, 0x56, 0x34, 0x12, 0x78, 0x56, 0x34, 0x12),
};

static void connected(struct bt_conn *conn, uint8_t err)
{
	if (err) {
		printk("Connection failed (err 0x%02x)\n", err);
	} else {
		printk("Connected\n");
	}
}

static void disconnected(struct bt_conn *conn, uint8_t reason)
{
	printk("Disconnected (reason 0x%02x)\n", reason);
}

static struct bt_conn_cb conn_callbacks = {
	.connected = connected,
	.disconnected = disconnected,
};

static void bt_ready(void)
{
	int err;

	printk("Bluetooth initialized\n");

	cts_init();

	if (IS_ENABLED(CONFIG_SETTINGS)) {
		settings_load();
	}

	err = bt_le_adv_start(BT_LE_ADV_CONN_NAME, ad, ARRAY_SIZE(ad), NULL, 0);
	if (err) {
		printk("Advertising failed to start (err %d)\n", err);
		return;
	}

	printk("Advertising successfully started\n");
}

static void bas_notify(unsigned int point)
{
	bt_bas_set_battery_level(point / 100);
}

// scale sensor value
int sensor_scale(const struct sensor_value *val, int factor)
{
	if(val->val1 >= INT16_MAX/factor) {
		return INT16_MAX;
	}
	if(val->val1 <= INT16_MIN/factor) {
		return INT16_MIN;
	}
	int value = val->val1*factor + (val->val2*factor)/1000000;
	if(value >= INT16_MAX) {
		return INT16_MAX;
	}
	if(value <= INT16_MIN) {
		return INT16_MIN;
	}
	return value;
};

static struct sensor_value temp, hum;
static void devhts_process_sample(const struct device *dev)
{
	static unsigned int obs;
	if (sensor_sample_fetch(dev) < 0) {
		printf("Sensor sample update error\n");
		return;
	}

	if (sensor_channel_get(dev, SENSOR_CHAN_AMBIENT_TEMP, &temp) < 0) {
		printf("Cannot read HTS221 temperature channel\n");
		return;
	}

	if (sensor_channel_get(dev, SENSOR_CHAN_HUMIDITY, &hum) < 0) {
		printf("Cannot read HTS221 humidity channel\n");
		return;
	}

	++obs;
	printf("\nObservation:%u, ", obs);

	/* display temperature */
	printf("Temperature:%.1f C, ", sensor_value_to_double(&temp));

	/* display humidity */
	printf("Relative Humidity:%.1f%%",
	       sensor_value_to_double(&hum));

	vnd_data.temperature = sensor_scale(&temp, 10);
	vnd_data.humidity = sensor_scale(&hum, 10);
}

static bool app_fw_2 = false;
static int devccs_do_fetch(const struct device *dev)
{
	struct sensor_value co2, tvoc, voltage, current;
	int rc = 0;
	int baseline = -1;

#ifdef CONFIG_APP_MONITOR_BASELINE
	rc = ccs811_baseline_fetch(dev);
	if (rc >= 0) {
		baseline = rc;
		rc = 0;
	}
#endif
	if (rc == 0) {
		rc = sensor_sample_fetch(dev);
	}
	if (rc == 0) {
		const struct ccs811_result_type *rp = ccs811_result(dev);

		sensor_channel_get(dev, SENSOR_CHAN_CO2, &co2);
		sensor_channel_get(dev, SENSOR_CHAN_VOC, &tvoc);
		sensor_channel_get(dev, SENSOR_CHAN_VOLTAGE, &voltage);
		sensor_channel_get(dev, SENSOR_CHAN_CURRENT, &current);
		printk("\n[%s]: CCS811: %u ppm eCO2; %u ppb eTVOC, ",
		       "now", co2.val1, tvoc.val1);
		printk("Voltage: %d.%06dV; Current: %d.%06dA", voltage.val1,
		       voltage.val2, current.val1, current.val2);

		vnd_data.co2 = sensor_scale(&co2, 1);
		vnd_data.voc = sensor_scale(&tvoc, 1);
#ifdef CONFIG_APP_MONITOR_BASELINE
		printk("BASELINE %04x\n", baseline);
#endif
		if (app_fw_2 && !(rp->status & CCS811_STATUS_DATA_READY)) {
			printk("STALE DATA\n");
		}

		if (rp->status & CCS811_STATUS_ERROR) {
			printk("ERROR: %02x\n", rp->error);
		}
	} else {
		printk("\ncould not fetch data from CCS.");
	}
	return rc;
}

static void devccs_trigger_handler(const struct device *dev,
			    struct sensor_trigger *trig)
{
	ccs811_envdata_update(dev, &temp, &hum);
	int rc = devccs_do_fetch(dev);

	if (rc == 0) {
		printk("Triggered fetch got %d\n", rc);
	} else if (-EAGAIN == rc) {
		printk("Triggered fetch got stale data\n");
	} else {
		printk("Triggered fetch failed: %d\n", rc);
	}
}

/** A discharge curve specific to the power source. */
static const struct battery_level_point levels[] = {
#if DT_NODE_HAS_PROP(DT_INST(0, voltage_divider), io_channels)
	/* "Curve" here eyeballed from captured data for the [Adafruit
	 * 3.7v 2000 mAh](https://www.adafruit.com/product/2011) LIPO
	 * under full load that started with a charge of 3.96 V and
	 * dropped about linearly to 3.58 V over 15 hours.  It then
	 * dropped rapidly to 3.10 V over one hour, at which point it
	 * stopped transmitting.
	 *
	 * Based on eyeball comparisons we'll say that 15/16 of life
	 * goes between 3.95 and 3.55 V, and 1/16 goes between 3.55 V
	 * and 3.1 V.
	 */

	{ 10000, 3950 },
	{ 625, 3550 },
	{ 0, 3100 },
#else
	/* Linear from maximum voltage to minimum voltage. */
	{ 10000, 3600 },
	{ 0, 1700 },
#endif
};



#define I2C_DEV DT_LABEL(DT_ALIAS(i2c_0))
#define BH1745_ADDR                 (0x38)	// Slave address "0111000" (ADDR = L )
#define BH1745_MANUFACTURER_ID_ADDR	(0x92)
#define BH1745_SYSTEM_CONTROL_ADDR	(0x40)
#define BH1745_MODE_CONTROL1_ADDR	(0x41)
#define BH1745_MODE_CONTROL2_ADDR	(0x42)
#define BH1745_MODE_CONTROL3_ADDR	(0x44)	// write 02h
#define BH1745_RED_DATA_LSB_ADDR	(0x50)
#define BH1745_RED_DATA_MSB_ADDR	(0x51)
#define BH1745_GREEN_DATA_LSB_ADDR	(0x52)
#define BH1745_BLUE_DATA_LSB_ADDR	(0x54)
#define BH1745_CLEAR_DATA_LSB_ADDR	(0x56)

#define BH1745_MANUFACTURER_ID_VAL  (0xE0)
#define BH1745_PART_ID_VAL          (0x0B)	// bitmask(5..0) on system_control


#define SX1509_ADDR (0x3e)


static int read_i2c_bytes(const struct device *i2c_dev, uint16_t addr,
		       uint8_t *data, uint32_t num_bytes)
{
	return i2c_burst_read(i2c_dev, BH1745_ADDR, addr, data, num_bytes);
}

static int write_i2c_bytes(const struct device *i2c_dev, uint16_t addr,
		       uint8_t *data, uint32_t num_bytes)
{
	return i2c_burst_write(i2c_dev, BH1745_ADDR, addr, data, num_bytes);
}



/* The devicetree node identifier for the "led0" alias. */
#define LED0_NODE DT_ALIAS(led0)
#define LED0	DT_GPIO_LABEL(LED0_NODE, gpios)
#define PIN0	DT_GPIO_PIN(LED0_NODE, gpios)
#define FLAGS0	DT_GPIO_FLAGS(LED0_NODE, gpios)

/*
#define LED2_NODE DT_ALIAS(led2)
#define LED2	DT_GPIO_LABEL(LED2_NODE, gpios)
#define PIN2	DT_GPIO_PIN(LED2_NODE, gpios)
#define FLAGS2	DT_GPIO_FLAGS(LED2_NODE, gpios)
*/


static struct nvs_fs fs;
#define NVS_POSITION_ID 1
static void persistency_init(void)
{
	struct flash_pages_info info;

	/* define the nvs file system by settings with:
	 *	sector_size equal to the pagesize,
	 *	3 sectors
	 *	starting at FLASH_AREA_OFFSET(storage)
	 */
	fs.offset = FLASH_AREA_OFFSET(storage);
	int rc = flash_get_page_info_by_offs(
		device_get_binding(DT_CHOSEN_ZEPHYR_FLASH_CONTROLLER_LABEL),
		fs.offset, &info);
	if (rc) {
		printk("Unable to get page info");
	}
	fs.sector_size = info.size;
	fs.sector_count = 3U;
	rc = nvs_init(&fs, DT_CHOSEN_ZEPHYR_FLASH_CONTROLLER_LABEL);
	if (rc) {
		printk("Flash Init failed\n");
	}

	/* ADDRESS_ID is used to store an address, lets see if we can
	 * read it from flash, since we don't know the size read the
	 * maximum possible
	 */
	uint8_t position;
	rc = nvs_read(&fs, NVS_POSITION_ID, &position, sizeof(position));
	if (rc > 0) { /* item was found, show it */
		printk("Id: %d, position: %d\n", NVS_POSITION_ID, (int)position);
	} else   {/* item was not found, add it */
		position = 0;
		printk("No position found, adding %d at id %d\n", (int)position, NVS_POSITION_ID);
		(void)nvs_write(&fs, NVS_POSITION_ID, &position, sizeof(position));
	}

}

static void position_write(uint8_t position)
{
	int err = nvs_write(&fs, NVS_POSITION_ID, &position, sizeof(uint8_t));
	printk("\nPosition write %d at id %d err: %d.\n", (int)position, NVS_POSITION_ID, (int)err);
}

static void position_read(uint8_t *position, uint8_t default_val)
{
	int rc = nvs_read(&fs, NVS_POSITION_ID, position, sizeof(uint8_t));
	if (rc > 0) { /* item was found, show it */
		printk("\npositio read: Id: %d, position: %d\n", NVS_POSITION_ID, (int)*position);
	} else   {/* item was not found, add it */
		*position = default_val;
		printk("No position found, adding %d at id %d\n", (int)*position, NVS_POSITION_ID);
		position_write(*position);
		nvs_write(&fs, NVS_POSITION_ID, position, sizeof(uint8_t));
	}
}


void main(void)
{
	int err;

	k_sleep(K_MSEC(500));
	printk("\nmain().\n");
	persistency_init();
	memset(&vnd_data, 0, sizeof(vnd_data));
	position_read(&vnd_data.sid, 0);

	err = bt_enable(NULL);
	if (err) {
		printk("Bluetooth init failed (err %d)\n", err);
		return;
	}
	

	bt_set_name("Environmento");
	bt_ready();

	bt_conn_cb_register(&conn_callbacks);

	const struct device *devled = device_get_binding(LED0);
	gpio_pin_configure(devled, PIN0, GPIO_OUTPUT_ACTIVE | FLAGS0);
	// const struct device *devledred = device_get_binding("Red LED");
	gpio_pin_configure(devled, 7, GPIO_OUTPUT_ACTIVE | FLAGS0);

	// temperature, humidity
	const struct device *devhts = device_get_binding("HTS221");

	// co2, voc
	const struct device *devccs = device_get_binding(DT_LABEL(DT_INST(0, ams_ccs811)));
	struct ccs811_configver_type cfgver;
	int rc = ccs811_configver_fetch(devccs, &cfgver);
	if (rc == 0) {
		printk("HW %02x; FW Boot %04x App %04x ; mode %02x\n",
		       cfgver.hw_version, cfgver.fw_boot_version,
		       cfgver.fw_app_version, cfgver.mode);
		app_fw_2 = (cfgver.fw_app_version >> 8) > 0x11;
	}
	rc = battery_measure_enable(true);
	if (rc != 0) {
		printk("Failed initialize battery measurement: %d\n", rc);
	}

	const struct device *i2c_dev = device_get_binding("I2C_0");
	if (!i2c_dev) {
		printk("I2C: Device driver not found.\n");
		return;
	}
	uint8_t data[10];
	rc =   (i2c_dev, BH1745_ADDR, BH1745_MANUFACTURER_ID_ADDR, data, 1);
	printk("\nManufacturer code: %02X, success:%d. ", (int)data[0], rc);
	rc = i2c_burst_read(i2c_dev, BH1745_ADDR, BH1745_SYSTEM_CONTROL_ADDR, &data[0], 3);
	printk("\npart code: %02X, CONTROL1:%02X, CONTROL2:%02X, success:%d. ", (int)data[0], (int)data[1], (int)data[2], rc);

	// light sensor
	data[0] = BH1745_SYSTEM_CONTROL_ADDR;	// Addr
	data[1] = 0x80;	// SW_RESET
	rc = i2c_write(i2c_dev, &data[0], 2, BH1745_ADDR);
	printk("\nreset device, success:%d. ", rc);
	k_sleep(K_MSEC(500));
	rc = i2c_burst_read(i2c_dev, BH1745_ADDR, BH1745_SYSTEM_CONTROL_ADDR, &data[0], 3);
	printk("\npart code: %02X, CONTROL1:%02X, CONTROL2:%02X, success:%d. ", (int)data[0], (int)data[1], (int)data[2], rc);

	data[0] = BH1745_MODE_CONTROL1_ADDR;	// Addr
	data[1] = 0; // no reset
	data[2] = 0x01;	// CONTROL1 320msec
	data[3] = 0x90;	// CONTROL2 enable, 1x
	data[4] = 0x00;	// 
	data[5] = 0x02;	// according data sheet
	// rc = i2c_write(i2c_dev, &data[0], 6, BH1745_ADDR);
	i2c_reg_write_byte(i2c_dev, BH1745_ADDR, BH1745_SYSTEM_CONTROL_ADDR, 0);	
	i2c_reg_write_byte(i2c_dev, BH1745_ADDR, BH1745_MODE_CONTROL1_ADDR, 0);	
	i2c_reg_write_byte(i2c_dev, BH1745_ADDR, BH1745_MODE_CONTROL2_ADDR, 0x92);	
	i2c_reg_write_byte(i2c_dev, BH1745_ADDR, BH1745_MODE_CONTROL3_ADDR, 2);	
	printk("\nactivate device, success:%d. ", rc);
	k_sleep(K_MSEC(500));
	rc = i2c_burst_read(i2c_dev, BH1745_ADDR, BH1745_SYSTEM_CONTROL_ADDR, &data[0], 3);
	printk("\npart code: %02X, CONTROL1:%02X, CONTROL2:%02X, success:%d. ", (int)data[0], (int)data[1], (int)data[2], rc);


	// enable LED PWM on io[7..5]-led-red
	i2c_reg_write_byte(i2c_dev, SX1509_ADDR, 0x21, 0xe0);	
	// setup PWM frequency
	i2c_reg_write_byte(i2c_dev, SX1509_ADDR, 0x1f, 0x70);	

	gpio_pin_set(devled, PIN0, 0);
	gpio_pin_set(devled, 7, 0);

	k_sleep(K_MSEC(500));

	int loop = 0;
	while (1) {
		++loop;
		// temperature, humidity
		devhts_process_sample(devhts);
		// co2, voc
		devccs_do_fetch(devccs);

		rc = i2c_burst_read(i2c_dev, BH1745_ADDR, BH1745_RED_DATA_LSB_ADDR, (uint8_t *)vnd_data.light, sizeof(vnd_data.light));
		if(rc == 0) {
			printk("\nlight sensor: RED:%d, GREEN:%d, BLUE:%d, CLEAR:%d. ", 
					(int)vnd_data.light[0], (int)vnd_data.light[1], (int)vnd_data.light[2], (int)vnd_data.light[3]);
		} else {
			printk("\nfailed to read light sensor. ");
		}

		/* Current Time Service updates only when time is changed */
		cts_notify();

		int batt_mV = battery_sample();
		if (batt_mV < 0) {
			printk("Failed to read battery voltage: %d\n", batt_mV);
		}
		unsigned int batt_pptt = battery_level_pptt(batt_mV, levels);
		printk("\nbattery %d mV; %u pptt\n", batt_mV, batt_pptt);
		/* Battery level simulation */
		bas_notify(batt_pptt);

		uint8_t ledval = (loop <<4) | 0x0f;
		// setup LED PWM fled-red value
		i2c_reg_write_byte(i2c_dev, SX1509_ADDR, 0x3B, ledval);		// green led
		i2c_reg_write_byte(i2c_dev, SX1509_ADDR, 0x40, ledval);		// blue led
		i2c_reg_write_byte(i2c_dev, SX1509_ADDR, 0x45, ledval);		// red led


		if(vnd_data.co2 <= CO2_YELLOW) {
			k_sleep(K_SECONDS(10));
			gpio_pin_set(devled, PIN0, 1);
			if(vnd_data.co2 > CO2_GREEN) {
				gpio_pin_set(devled, 7, 1);
			}
			k_sleep(K_MSEC(70));
			gpio_pin_set(devled, PIN0, 0);
			if(vnd_data.co2 > CO2_GREEN) {
				gpio_pin_set(devled, 7, 0);
			}
		} else if(vnd_data.co2 <= CO2_RED) {
			k_sleep(K_SECONDS(5));
			gpio_pin_set(devled, 7, 1);
			k_sleep(K_MSEC(70));
			gpio_pin_set(devled, 7, 0);
		} else {
			k_sleep(K_SECONDS(1));
			gpio_pin_set(devled, 7, 1);
			k_sleep(K_MSEC(70));
			gpio_pin_set(devled, 7, 0);
		}

	}
}
