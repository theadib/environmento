development blog
================


implementing light sensor
-------------------------

The light sensor BH1745 is not supported by Zephyr out of the box.
- https://docs.zephyrproject.org/latest/boards/arm/thingy52_nrf52832/doc/index.html

Instead the driver has to be included into the project.

For the underlaying layout @see 
- https://stackoverflow.com/questions/62358554/zephyros-how-to-add-driver-module-to-out-of-tree-project
- https://interrupt.memfault.com/blog/building-drivers-on-zephyr

nordic docuemnatation:
- https://developer.nordicsemi.com/nRF_Connect_SDK/doc/latest/nrf/gs_installing.html

Copy the sources from nordic sdk:
- https://github.com/nrfconnect/sdk-nrf

